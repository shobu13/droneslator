package main

import (
	"droneslator/formaters"
	"droneslator/profiles"
	"droneslator/translation"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/urfave/cli/v2"
	"golang.org/x/exp/maps"
)

func main() {
	err := (&cli.App{
		Name:  "droneslator",
		Usage: "droneslator [TEXT] [PROTOCOL]",

		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:    "profile",
				Aliases: []string{"P"},
				Value:   "default",
				Usage:   "overwrite profile to user",
			},
		},

		Commands: []*cli.Command{
			{
				Name:        "translate",
				Aliases:     []string{"tr"},
				Usage:       "translate a human string to a drone output",
				Description: "[TEXT] text to translate\n[PROTOCOL] protocol to print in the protocol section of the translated sentence",
				ArgsUsage:   "[PROTOCOL] [TEXT]",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:    "section_delimiter",
						Aliases: []string{"D"},
						Value:   "::",
						Usage:   "overwrite the delimiter of each sentence section",
					},
					&cli.StringFlag{
						Name:    "delimiter",
						Aliases: []string{"d"},
						Value:   " ",
						Usage:   "overwrite the delimiter between two words",
					},
					&cli.StringFlag{
						Name:    "prefix",
						Aliases: []string{"p"},
						Usage:   "overwrite the prefix of the sentence (usually the drone ID)",
					},
				},
				Action: translation.TranslateString,
			},
			{
				Name:    "formaters",
				Aliases: []string{"fmts"},
				Usage:   "formaters related operations",
				Subcommands: []*cli.Command{
					{
						Name:  "list",
						Usage: "list available formaters",
						Action: func(ctx *cli.Context) error {
							for key, _ := range formaters.FORMATERS {
								println("-", key)
								println("\tUsage:", formaters.FORMATERS[key].Usage)
								if len(formaters.FORMATERS[key].Args) != 0 {
									println("\tAvailable configs")
									for _, config := range formaters.FORMATERS[key].Args {
										println("\t\t-", config.Name, ":"+config.Usage)
									}
								}
							}
							return nil
						},
					},
					{
						Name:        "add",
						Usage:       "add the given formatter",
						Description: fmt.Sprintf("[FORMATTER] name of the formater to add (%v)", strings.Join(maps.Keys(formaters.FORMATERS), ", ")),
						ArgsUsage:   "[FORMATTER]",
						Action:      profiles.AddActiveFormater,
					},
					{
						Name:   "actives",
						Usage:  "list the current actives formaters",
						Action: profiles.ListActiveFormaters,
					},
					{
						Name:        "remove",
						Usage:       "remove the active formater at the given index",
						Description: "[INDEX] index of the formater to remove (droneslator fmts actives)",
						ArgsUsage:   "[INDEX]",
						Action:      profiles.RemoveActiveFormater,
					},
					{
						Name:        "shift",
						Aliases:     []string{"s"},
						Usage:       "remove the active formater at the given index",
						Description: "[INDEX] index of the active formater to shift\n ",
						ArgsUsage:   "[INDEX]",
						Subcommands: []*cli.Command{
							{
								Name:        "up",
								Aliases:     []string{"u"},
								Usage:       "shift the given formater one index up",
								Description: "[INDEX] index of the active formater to shift\n ",
								ArgsUsage:   "[INDEX]",
								Action: func(context *cli.Context) error {
									return profiles.ShiftActiveFormater(context, -1)
								},
							},
							{
								Name:        "down",
								Aliases:     []string{"d"},
								Usage:       "shift the given formater on index down",
								Description: "[INDEX] index of the active formater to shift\n ",
								ArgsUsage:   "[INDEX]",
								Action: func(context *cli.Context) error {
									return profiles.ShiftActiveFormater(context, 1)
								},
							},
						},
					},
				},
			},
		},
	}).Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
