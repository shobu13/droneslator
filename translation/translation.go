package translation

import (
	"droneslator/formaters"
	"droneslator/profiles"
	"fmt"
	"strings"

	"github.com/urfave/cli/v2"
)

func TranslateString(ctx *cli.Context) error {
	config := profiles.LoadConfig()

	profile := config.GetCurrentProfile()
	if profile.Prefix == "" {
		profile.Prefix = ctx.String("prefix")
	}
	inputString := ctx.Args().Get(1)

	words := strings.Split(inputString, " ")

	for _, formaterConfig := range profile.Formaters {
		for index, word := range words {
			if len(word) > 5 && word[:2] == "{#" && word[len(word)-2:] == "#}" {
				words[index] = word
			} else {
				words[index] = formaters.FORMATERS[formaterConfig.Type].Format(word, formaterConfig.Config)
			}
		}
	}

	inputString = strings.Join(words, profile.Delimiter)
	inputString = strings.ReplaceAll(inputString, "{#", "")
	inputString = strings.ReplaceAll(inputString, "#}", "")

	fmt.Printf("%[2]v%[1]v%[3]v%[1]v%[4]v\n", profile.SectionDelimiter, profile.Prefix, ctx.Args().Get(0), inputString)

	return nil
}
