# DRONESLATOR

a cli tool to help writing in character sentence for Drones players

# Installation

## binaries

go to the latest [release](https://gitlab.com/shobu13/droneslator/-/releases/permalink/latest/) and download the binary for your OS  

copy the downloaded binary to the folder of your choice (usually one in your $PATH)

## golang cli

`git clone https://gitlab.com/shobu13/droneslator.git`  
`cd droneslator`  
`go install`

# Configuration

create a file named `.droneslator.json` in your home directory ex: `/home/user/.droneslator.json`  
paste the folowing:

```json
{
    "profiles": {
        "default": {
        "prefix": "",
        "section_delimiter": "::",
        "delimiter": "  ",
        "formaters": [
            {
                "type": "upper"
            },
            {
                "type": "split",
                "config": {
                    "delimiter": " "
                }
            }
        ]
    }
    }

}
```

sentences are formated usings this parameters, curently, you can't customize the order of the sections

## section order

`{{PREFIX}}{{SECTION_DELIMITER}}{{PROTOCOL}}{{SECTION_DELIMITER}}{{TRANSLATED_MESSAGE}}`

## configurations variables

**prefix**: the prefix prepend to all of your sentences, usually your drone identifier ex: <u>2267</u>::...  
**section_delimiter**: delimiter between each section ex: {{PREFIX}}<u>::</u>{{PROTOCOL}}  
**delimiter**: delimiter between two words of the translated sentence ex: HELLO<u>&nbsp;&nbsp;</u>WORLD  
**formaters**: list and configurations of the current activated sentence formatter, curently you've got access to only two:

* upper: uppercase all letters of the sentence
  * ex: hello -> HELLO
* split: split each word with the provided delimiter
  * ex: hello -> h e l l o
    each formater is applied to each word of the sentence one after the other
    you can wrap a word with {#word#} to prevent it to be formated

# Usage

`droneslator tr -p 2267 SHARING "link here: {#https://gitlab.com/shobu13/droneslator#}"`  
`>>> 2267::SHARING::L I N K  H E R E :  https://gitlab.com/shobu13/droneslator`