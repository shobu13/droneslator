package formaters

import (
	"math/rand"
	"strconv"
	"strings"
)

type Config struct {
	Name    string
	Usage   string
	Default string
}

type FormaterArguments struct {
	Name    string
	Usage   string
	Default string
}

type Formater struct {
	Usage  string
	Format func(word string, config map[string]string) string
	Args   []FormaterArguments
}

func (formater Formater) ArgsList() []string {
	result := []string{}
	for _, arg := range formater.Args {
		result = append(result, arg.Name)
	}
	return result
}

var FORMATERS = map[string]Formater{
	"upper": {
		Format: func(word string, config map[string]string) string {
			return strings.ToUpper(word)
		},
		Usage: "Uppercase each word",
	},
	"split": {
		Format: func(word string, config map[string]string) string {
			return strings.Join(strings.Split(word, ""), config["delimiter"])
		},
		Usage: "Split each word by a provided 'delimiter'. ex: hive -> h#i#v#e",
		Args: []FormaterArguments{
			{
				Name:    "delimiter",
				Usage:   "character to be inserted between each letter. ex: #",
				Default: " ",
			},
		},
	},
	"swap": {
		Format: func(word string, config map[string]string) string {
			return strings.ReplaceAll(word, config["old"], config["new"])
		},
		Usage: "replace all provided character with a new character. ex: hive -> h#ve",
		Args: []FormaterArguments{
			{
				Name:    "old",
				Usage:   "character to replace. ex: i",
				Default: " ",
			},
			{
				Name:    "new",
				Usage:   "character to swap with the old one. ex: #",
				Default: "#",
			},
		},
	},
	"glitch": {
		Format: func(word string, config map[string]string) string {

			ratio, err := strconv.Atoi(config["ratio"])

			if err != nil {
				panic(err)
			}

			if ratio > 100 || ratio < 0 {
				panic("config glitch.ratio must exist and between 0-100")
			}

			word_ratio, err := strconv.Atoi(config["word_ratio"])

			if err != nil {
				panic(err)
			}

			if word_ratio > 100 || word_ratio < 0 {
				panic("config glitch.word_ratio must exist and between 0-100")
			}

			if rand.Intn(100) < ratio {
				newWord := ""
				for _, char := range word {
					if rand.Intn(100) < word_ratio {
						newWord += string(config["characters"][rand.Intn(len(config["characters"]))])
					} else {
						newWord += string(char)
					}
				}
				word = newWord
			}
			return word
		},
		Usage: "",
		Args: []FormaterArguments{
			{
				Name:    "ratio",
				Usage:   "ratio of glitching between 0-100. Determine if a word will be glitched in a translated sentence. ex: A ratio of 50 will mean than 50% of the words will be glitched.",
				Default: "20",
			},
			{
				Name:    "word_ratio",
				Usage:   "ratio of glitching between 0-100. Determine if a letter will be glitched in a glitched word, ex: A ratio of 50 will mean than 50% of the letter in a glitched word will be glitched.",
				Default: "20",
			},
			{
				Name:    "characters",
				Usage:   "an array of characters to use for glitching a translated sentence.",
				Default: "#@$%",
			},
		},
	},
}
