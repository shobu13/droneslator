package profiles

import (
	"droneslator/formaters"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"strconv"

	_ "embed"

	"github.com/urfave/cli/v2"
)

//go:embed baseConfig.json
var baseConfigRaw []byte

func LoadConfig() JsonConfig {
	var jsonFile *os.File
	var jsonConfig JsonConfig

	homePath, _ := os.UserHomeDir()

	dirPath := path.Join(homePath, ".droneslator"+".json")
	_, err := os.Stat(dirPath)
	if os.IsNotExist(err) {
		fmt.Println("no config file found in " + dirPath + " creating one using default config.")
		err = os.WriteFile(dirPath, baseConfigRaw, 0666)
		if err != nil {
			log.Fatal(err)
		}
		jsonFile, _ = os.Open(dirPath)
	} else {
		jsonFile, err = os.Open(dirPath)
		if err != nil {
			log.Fatal(err)
		}
	}

	bytes, _ := io.ReadAll(jsonFile)
	json.Unmarshal(bytes, &jsonConfig)

	return jsonConfig
}

func WriteConfig(config JsonConfig) {
	homePath, _ := os.UserHomeDir()

	path := path.Join(homePath, ".droneslator"+".json")

	jsonFile, err := json.Marshal(config)

	if err != nil {
		log.Fatal(err)
	}

	err = os.WriteFile(path, jsonFile, 0666)

	if err != nil {
		log.Fatal(err)
	}
}

func (jsonConfig JsonConfig) GetProfile(profileName string) *UserProfile {

	profile := jsonConfig.Profiles[profileName]

	fmt.Println("using profile: " + profileName)

	return profile
}

func (jsonConfig JsonConfig) GetCurrentProfile() *UserProfile {
	if jsonConfig.CurrentProfile != "" {
		return jsonConfig.GetProfile(jsonConfig.CurrentProfile)
	} else {
		return jsonConfig.GetProfile("default")
	}
}

func (profile UserProfile) PrintFormaters() {
	for index, formater := range profile.Formaters {
		fmt.Printf("[%v] - %v\n", index, formater.Type)
		for key, value := range formater.Config {
			fmt.Printf("\t%v : '%v'\n", key, value)
		}
	}
}

func ListActiveFormaters(ctx *cli.Context) error {
	config := LoadConfig()
	config.GetCurrentProfile().PrintFormaters()
	return nil
}

func RemoveActiveFormater(ctx *cli.Context) error {
	config := LoadConfig()
	profile := config.GetCurrentProfile()
	index, err := strconv.ParseInt(ctx.Args().Get(0), 10, 0)

	if err != nil ||
		(index < 0) ||
		(int(index) > len(profile.Formaters)) {
		log.Panicf("Incorrect argument %v for delete (must be a postive integer between 0 and %v)\n",
			index, len(profile.Formaters))
	}

	profile.Formaters = append(profile.Formaters[:index], profile.Formaters[index+1:]...)

	WriteConfig(config)

	return nil
}

func AddActiveFormater(ctx *cli.Context) error {
	formaterName := ctx.Args().Get(0)
	formater := formaters.FORMATERS[formaterName]
	argsNames := formater.ArgsList()

	argsConfig := map[string]string{}

	config := LoadConfig()
	profile := config.GetCurrentProfile()

	fmt.Println("add formater", formaterName)
	for _, argName := range argsNames {
		var value string
		fmt.Println("value for config [", argName, "]: ")
		fmt.Scanln(&value)
		argsConfig[argName] = value
	}

	profile.Formaters = append(profile.Formaters, FormaterConfig{
		Type:   formaterName,
		Config: argsConfig,
	})

	WriteConfig(config)

	return nil
}

func ShiftActiveFormater(ctx *cli.Context, shift int) error {
	// shift the index of the given active formater down or up
	config := LoadConfig()
	profile := config.GetCurrentProfile()

	index, err := strconv.ParseInt(ctx.Args().Get(0), 10, 0)

	if err != nil ||
		(int(index) > len(profile.Formaters)) ||
		(int(index) < 0) {
		return errors.New(fmt.Sprintf("Incorrect argument (index) %v for shifting (must be a postive integer between 0 and %v)\n",
			index, len(profile.Formaters)))
	}

	if (shift+int(index) > len(profile.Formaters)-1) ||
		(shift+int(index) < 0) {
		return errors.New(fmt.Sprintf("Incorrect argument (shift) %v for shifting formater of index %v (must be a postive integer between %v and %v)\n",
			shift, index, index*-1, (len(profile.Formaters)-int(index))-1))
	}

	prev := profile.Formaters[int(index)+shift]
	profile.Formaters[int(index)+shift] = profile.Formaters[index]
	profile.Formaters[index] = prev

	WriteConfig(config)

	profile.PrintFormaters()

	return nil
}
