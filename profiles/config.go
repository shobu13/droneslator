package profiles

type JsonConfig struct {
	Profiles       map[string]*UserProfile `json:"profiles"`
	CurrentProfile string                  `json:"current"`
}

type FormaterConfig struct {
	Type   string            `json:"type"`
	Config map[string]string `json:"config"`
}

type UserProfile struct {
	Prefix           string           `json:"prefix"`
	SectionDelimiter string           `json:"section_delimiter"`
	Delimiter        string           `json:"delimiter"`
	Formaters        []FormaterConfig `json:"formaters"`
}
